package se.miun.dsv.jee.jpa;

import junit.framework.*;
import junit.textui.*;

/**
 * {Insert class description here}
 *
 * 2015/11/18 
 * @author <a href="mailto:osre1300@student.miun.se">Oscar Reimer</a><br><a href="mailto:mabo0920@student.miun.se">Martin Book</a>
 */

public class UnitTestRunner {
    private static void run(Class<?> clazz) {
        System.out.println("Running unit tests for " + clazz);
        TestRunner.run(new JUnit4TestAdapter(clazz));
    }

    public static void run() {
        MySecurityManager sm = new MySecurityManager();
        Class<?> clazz = sm.getClassContext()[2];
        run(clazz);
    }

    private static class MySecurityManager extends SecurityManager {
        @Override
        public Class<?>[] getClassContext() {
            return super.getClassContext();
        }
    }
}
