package se.miun.dsv.jee.jpa;

import database.Event;
import database.User;
import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 * {Insert class description here}
 *
 * 2015/11/10 
 * @author <a href="mailto:osre1300@student.miun.se">Oscar Reimer</a><br><a href="mailto:mabo0920@student.miun.se">Martin Book</a>
 */

public class ImportVerifier implements IImportVerifier
{
    protected final EntityManagerFactory eMF = Persistence
            .createEntityManagerFactory("events");
    
    @Override
    public List<Event> findAllEventsThatOverlapWithOthers()
    {
        Set<Event> overlappingEvents = new LinkedHashSet<>();
        EntityManager eM = eMF.createEntityManager();
        
        List<Event> events = eM.createNamedQuery("Events.findAll").getResultList();
        
        eM.close();
        
        for(int i = 0; i < events.size(); i++)
        {
            Event curEvent = events.get(i);
            
            for (int j = i + 1; j < events.size(); j++)
            {
                Event compEvent = events.get(j);
                if (curEvent.getStartTime().compareTo(compEvent.getStartTime()) <= 0
                        && curEvent.getEndTime().compareTo(compEvent.getStartTime()) >= 0)
                {
                    overlappingEvents.add(curEvent);
                    overlappingEvents.add(compEvent);
                }
                else if(curEvent.getStartTime().compareTo(compEvent.getStartTime()) <= 0
                        && curEvent.getEndTime().compareTo(compEvent.getEndTime()) >= 0)
                {
                    overlappingEvents.add(curEvent);
                    overlappingEvents.add(compEvent);
                }
                else if (compEvent.getStartTime().compareTo(curEvent.getStartTime()) <= 0
                        && compEvent.getEndTime().compareTo(curEvent.getStartTime()) >= 0)
                {
                    overlappingEvents.add(curEvent);
                    overlappingEvents.add(compEvent);
                }
                else if(compEvent.getStartTime().compareTo(curEvent.getStartTime()) <= 0
                        && compEvent.getEndTime().compareTo(curEvent.getEndTime()) >= 0)
                {
                    overlappingEvents.add(curEvent);
                    overlappingEvents.add(compEvent);
                }
            }
        }
        List<Event> returnEvents = new LinkedList<>();
        returnEvents.addAll(overlappingEvents);
        return returnEvents;
    }

    @Override
    public List<String> findFullNamesOfUsersHostingFutureEvents()
    {
        EntityManager eM = eMF.createEntityManager();
        
        TypedQuery<Event> q = eM.createQuery("SELECT DISTINCT e FROM Event e WHERE e.startTime >= :providedDateTime", Event.class);
        
        
        List<Event> events = q.setParameter("providedDateTime", LocalDateTime.now()).getResultList();
        
        List<String> names = new LinkedList<>();
        for (Event event: events)
        {
            Set<User> users = event.getUsers();
            for(User usr : users )
            {
                names.add(usr.getFirstName() + " " + usr.getLastName());
            }
        }
        
        eM.close();
        
        return names;
    }

    @Override
    public Long findNumberOfUsersWithMoreThanOneComment()
    {
        EntityManager eM = eMF.createEntityManager();
        
        TypedQuery<String> q = eM.createQuery("SELECT c.user.id FROM Comment c GROUP BY c.user.id HAVING count(c.user.id) > 1", String.class);
        
        List<String> users = q.getResultList();
        
        eM.close();
        
        return (long)users.size();
    }

    @Override
    public List<Event> findPastEventsInHamburg()
    {
        EntityManager eM = eMF.createEntityManager();
        
        TypedQuery<Event> q = eM.createQuery("SELECT DISTINCT e FROM Event e WHERE e.startTime < :providedDateTime AND e.city = :providedCity", Event.class);
        
        
        List<Event> events = q.setParameter("providedDateTime", LocalDateTime.now())
                .setParameter("providedCity", "Hamburg")
                .getResultList();
        
        eM.close();
        
        return events;
    }
    
}
